package Test::DummyResultSet;
use Moose;
use namespace::autoclean;

has search_args => (
    is => 'rw',
    isa => 'ArrayRef',
    default => sub { [] },
);

has hstore_column => (
    is => 'ro',
    isa => 'Str',
    default => 'hstore_properties',
);

has text_vector_column => (
    is => 'ro',
    isa => 'Str',
    default => 'text_vector'
);

sub result_source {
    my $mock_dbh = Test::MockObject->new();
    $mock_dbh->mock('quote' => sub {
        my $self = shift;
        return sprintf(q{%s (quoted)}, $_[0]);
    });

    my $mock_storage = Test::MockObject->new();
    $mock_storage->set_always('dbh' => $mock_dbh);

    my $mock_source = Test::MockObject->new();
    $mock_source->set_always('storage' => $mock_storage);
    $mock_source->mock('primary_columns', sub { return ('uuid') });

    return $mock_source;
}

sub search {
    my $self = shift;
    $self->search_args(
        [
            @{ $self->search_args },
            [@_]
        ]
    );
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

