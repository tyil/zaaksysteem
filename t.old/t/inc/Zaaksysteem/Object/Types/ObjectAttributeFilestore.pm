package Zaaksysteem::Object::Types::ObjectAttributeFilestore;

use Moose;

extends 'Zaaksysteem::Object';

has profile_picture => (
    is => 'rw',
    isa => 'ArrayRef[HashRef]',
    traits => [qw[OA]],
    label => 'A profile picture of hasselhoff',
    type => 'file'
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
