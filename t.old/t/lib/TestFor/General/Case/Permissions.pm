package TestFor::General::Case::Permissions;
use base qw(Test::Class);

use TestSetup;


sub create_testcase {
    my $properties = shift;

    my $zaaktype_node = $zs->create_zaaktype_node_ok(
        properties => $properties,
    );

    my $zaaktype_status = $zs->create_zaaktype_status_ok(
        status => 1,
        fase   => 'registratiefase',
        node   => $zaaktype_node
    );

    my $afhandelfase = $zs->create_zaaktype_status_ok(
        status => 2,
        fase   => 'afhandelfase',
        node   => $zaaktype_node
    );

    my $casetype = $zs->create_zaaktype_ok(node => $zaaktype_node);
    return $zs->create_case_ok(zaaktype => $casetype);
}

sub check_queue_coworker_changes : Tests {
    $zs->zs_transaction_ok(sub {
        $zs->set_current_user;
        my $case = create_testcase({ queue_coworker_changes => 0 });

        ok !$case->check_queue_coworker_changes(1), "By default no queue";

        $case = create_testcase({ queue_coworker_changes => 1 });

        ok $case->check_queue_coworker_changes(1), "Queue when case not opened and user_id given";

        $case->open_zaak;

        my $behandelaar_id = $case->behandelaar->gegevens_magazijn_id;

        ok !$case->check_queue_coworker_changes($behandelaar_id),
            "No queuing when active user is behandelaar";

        # different user id on purpose
        ok $case->check_queue_coworker_changes($behandelaar_id + 1),
            "Queuing when active user is not behandelaar";

    }, 'Prove queue_coworker_changes');
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

