#!/usr/bin/env perl
use strict;
use warnings;

use Cwd 'realpath';
use FindBin;
use lib "$FindBin::Bin/../lib";

use Zaaksysteem::CLI::NatuurlijkPersoon::AltAuth;

my $cli = Zaaksysteem::CLI::NatuurlijkPersoon::AltAuth->init();
$cli->run();

1;

__END__

=head1 NAME

zs-alt-auth.pl - Fixup alternative authentication accounts

=head1 SYNOPSIS

zs-alt-auth.pl OPTIONS

=head1 OPTIONS

=over

=item hostname

The hostname of the instance you want to run the script for

=item dry

If you don't want to commit the changes

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
