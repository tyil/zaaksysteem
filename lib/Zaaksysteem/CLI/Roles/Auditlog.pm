package Zaaksysteem::CLI::Roles::Auditlog;
use Moose::Role;

=head1 NAME

Zaaksysteem::CLI::Roles::Auditlog - Generate auditlogs

=head1 DESCRIPTION

Role for creating audit logs.

=head1 SYNOPSIS

    package ZS::CLI::Foo;
    with 'Zaaksysteem::CLI::Roles::Auditlog';

    $self->audit_log(qw(audit data you want to store in CSV format));
    $self->error_log(qw(error data you want to store in CSV format));

=cut

use IO::Handle;
use DateTime;

=head1 ATTRIBUTES

=cut

has audit_log_path => (
    is      => 'ro',
    isa     => 'Str',
    lazy    => 1,
    default => sub { my $self = shift; return join('-', 'output', $self->hostname, DateTime->now->ymd) . ".csv" },
);

has error_log_path => (
    is      => 'ro',
    isa     => 'Str',
    lazy    => 1,
    default => sub { my $self = shift; return join('-', 'error', $self->hostname, DateTime->now->ymd) . ".csv" },
);

has csv => (
    is      => 'ro',
    isa     => 'Text::CSV',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return Text::CSV->new({ binary => 1, eol => "\015\012" });
    }
);

has _error_fh => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return $self->to_fh($self->error_log_path);
    }
);

has _audit_fh => (
    is      => 'ro',
    lazy    => 1,
    default => sub {
        my $self = shift;
        return $self->to_fh($self->audit_log_path);
    }
);

=head1 METHODS

=head2 audit_log

Write data to your audit log

=cut

sub audit_log {
    my $self = shift;
    $self->csv->print($self->_audit_fh, \@_);
}

=head2 error_log

Write data to your error log

=cut

sub error_log {
    my $self = shift;
    $self->csv->print($self->_error_fh, \@_);
}

=head2 to_fh

Get a path and opens the filehandle. Enables autoflush. Returns the filehandle.

=cut

sub to_fh {
    my ($self, $path) = @_;
    use autodie;
    open my $log, ">", $path;
    $log->autoflush(1);
    no autodie;
    return $log;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
