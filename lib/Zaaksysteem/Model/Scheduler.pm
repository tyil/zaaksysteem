package Zaaksysteem::Model::Scheduler;
use Moose;
use namespace::autoclean;

extends 'Catalyst::Model::Factory::PerRequest';

=head1 NAME

Zaaksysteem::Model::Scheduler - Model for the job scheduler

=head1 SYNOPSIS

    # In a controller:
    $c->model('Scheduler')->run_pending();

=cut

__PACKAGE__->config(
    class       => 'Zaaksysteem::Scheduler',
    constructor => 'new',
);

sub prepare_arguments {
    my ($self, $c) = @_;

    my %args = (c => $c);

    return \%args;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 prepare_arguments

TODO: Fix the POD

=cut

