package Zaaksysteem::Model::ObjectMutation;

use Moose;
use namespace::autoclean;

# case context makes this uncacheable
extends 'Catalyst::Model::Factory';

__PACKAGE__->config(
    class => 'Zaaksysteem::Object::Mutation::Model',
    constructor => 'new'
);

=head1 NAME

Zaaksysteem::Model::ObjectMutation - Catalyst model factory for
L<Zaaksysteem::Object::Mutation::Model>.

=head1 SYNOPSIS

    my $mutation_model = $c->model('ObjectMutation', {
        zaak => $my_zaak_component_row,
        [ resultset => $my_object_mutation_rs ]
    });

=head1 ARGUMENTS

=head2 zaak

This required argument is required to contextualize the object mutation
resultset, so that operations on the resultset happen in within the confines
of one case.

=cut

use BTTW::Tools;

=head1 METHODS

=head2 prepare_arguments

Prepares the instantiation arguments for
L<<Zaaksysteem::Object::Mutation::Model->new>>.

=cut

define_profile prepare_arguments => (
    required => {
        zaak => 'Zaaksysteem::Zaken::ComponentZaak'
    },
    optional => {
        mutation_resultset => 'DBIx::Class::ResultSet',
        logging_resultset => 'DBIx::Class::ResultSet',
    },
);

sub prepare_arguments {
    my $self = shift;
    my $c = shift;
    my $args = assert_profile(shift)->valid;

    if (scalar @_) {
        throw(
            'object/model/mutation/extra_arguments',
            'ObjectMutation model factory received too many arguments'
        );
    }

    my $zaak = $args->{ zaak };

    my $mutation_rs  = $args->{ mutation_resultset }
                    || $c->model('DB::ObjectMutation');

    my $logging_rs  = $args->{ logging_resultset }
                   || $c->model('DB::Logging');

    $mutation_rs = $mutation_rs->search({
        lock_object_uuid => $zaak->get_column('uuid')
    });

    $logging_rs = $logging_rs->search({
        zaak_id => $zaak->id
    });

    return {
        mutation_resultset => $mutation_rs,
        logging_resultset => $logging_rs,
        object_model => $c->model('Object'),
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
