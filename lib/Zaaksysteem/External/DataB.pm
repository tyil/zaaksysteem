package Zaaksysteem::External::DataB;

use Moose;
use namespace::autoclean;

with qw(
    MooseX::Log::Log4perl
    Zaaksysteem::Roles::UserAgent
);

=head1 NAME

Zaaksysteem::External::DataB - An interface for DataB calls

=head1 DESCRIPTION

Takes care of all the calls used for the DataB interface

=cut

use BTTW::Tools;
use Digest::MD5 qw(md5_hex);
use HTTP::Request;
use MIME::Base64 qw[encode_base64];
use URI;
use XML::XPath;
use Zaaksysteem::Types qw[
    SecureHTTP_URI
];

=head1 SYNOPSIS

    use Zaaksysteem::External::DataB;

    my $datab = Zaaksysteem::External::DataB->new(
        cid => 'my_customer_id',
        secret => 'shared_secret_key',
        eindpoint => 'https://somewhere.tld/api/endpoint',
        subject_magic_string => 'bsn',
    );

    my $invitation_url = $datab->get_session_url();

=head1 ATTRIBUTES

=head2 cid

The customer ID on for the DataB customer

=head2 secret

The shared secret

=head2 endpoint

The endpoint for MultiChannel DataB

=head2 subject_magic_string

Magic string template for that should dereference the subject's identifying
value for the MultiChannel application.

=head2 spoof_mode

Sets the model to spoof-mode. In this mode, the main production method
L</get_session_url> returns a data URI with a spoofed landing page, allowing
the tester to verify the arguments that would have been used to request a
user session from DataB.

=cut

has cid => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has secret => (
    is       => 'ro',
    isa      => 'Str',
    required => 1,
);

has endpoint => (
    is       => 'ro',
    isa      => SecureHTTP_URI,
    coerce   => 1,
    required => 2,
);

has subject_magic_string => (
    is       => 'ro',
    isa      => 'Str',
    required => 1
);

has spoof_mode => (
    is => 'ro',
    isa => 'Bool',
    default => 0
);

has _ztt_cache => (
    is => 'rw',
    isa => 'HashRef',
    default => sub { return {}; }
);

=head1 METHODS

=head2 get_session_url

Get the session URL for the client

=cut

define_profile get_session_url => (
    required => {
        subject    => 'Zaaksysteem::Betrokkene::Object',
        useragent  => 'Str'
    }
);

sub get_session_url {
    my $self      = shift;
    my $params    = assert_profile(shift)->valid;

    if ($self->spoof_mode) {
        return $self->_generate_spoof_url($params->{ subject });
    }

    my $res = $self->ua->request($self->build_session_request(%$params));
    my $xml = $self->assert_http_response($res);
    return $self->assert_session_xml($xml);
}

=head2 assert_session_xml

Get the session response URI from the XML returned by MultiChannel/DataB

=cut

sub assert_session_xml {
    my ($self, $xml) = @_;

    my $xpath = XML::XPath->new(xml => $xml);

    unless ($xpath->findvalue('/docserver/response/succes') eq 'true') {
        throw(
            'sysin/multichannel/request_unsuccessful',
            sprintf('MultiChannel session URL request did not succeed: %s',
                $xpath->findvalue('/docserver/response/error/@description')
                    ->value)
        );
    }

    return $xpath->findvalue('/docserver/response/url');
}

=head2 build_session_request

Helper method for constructing a MultiChannel Authentication request. Returns
an instance of L<HTTP::Request> for that purpose.

=cut

define_profile build_session_request => (
    required => {
        subject    => 'Zaaksysteem::Betrokkene::Object',
        useragent  => 'Str'
    }
);

sub build_session_request {
    my $self      = shift;
    my $params    = assert_profile({@_})->valid;

    my $hash = md5_hex($params->{useragent});

    my $uri = $self->endpoint->clone;

    $uri->query_form(
        {
            cid     => $self->cid,
            secret  => $self->secret,
            request => 'authenticate',
            clhash  => $hash,
            uid     => $self->_eval_magic_string($params->{ subject }),
        }
    );

    my $req = HTTP::Request->new('GET');
    $req->uri($uri);
    return $req;
}

sub _generate_spoof_url {
    my ($self, $subject) = @_;

    my $subject_str = sprintf(
        '%s, "%s", multichannel_id = "%s"',
        $subject->betrokkene_identifier,
        $subject->display_name,
        $self->_eval_magic_string($subject),
    );

    my $html = sprintf(<<END_OF_HTML, $subject_str);
<html>
<head>
    <title>Spoofmode DataB MultiChannel landing page</title>
</head>
<body>
    <h1>Spoofmode DataB MultiChannel landing page</h1>
    WOZ voor %s
</body>
</html>
END_OF_HTML

    return sprintf('data:text/html;charset=utf-8;base64,%s', encode_base64($html));
}

sub _eval_magic_string {
    my ($self, $subject) = @_;

    my $ztt = Zaaksysteem::ZTT->new(cache => $self->_ztt_cache);

    $ztt->add_context($subject);

    my $tpl = $ztt->process_template($self->subject_magic_string);

    return $tpl->string;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
