package Zaaksysteem::ZAPI::CRUD::Interface::Column;

use Moose;

use constant OBJECT_PROFILE     => {
    required            => [qw/
        id
        label
    /],
    optional            => [qw/
        resolve
        sort
        filter
        template
        dynamic
    /],
    constraint_methods  => {
        id              => qr/^[\w_-]+/,
        label           => qr/^[\w\s-]+/,
        resolve         => qr/^[\w\.]+/,
        sort            => sub {
            my ($dfv)   = @_;

            my $val     = $dfv->get_filtered_data->{
                $dfv->get_current_constraint_field
            };

            return unless UNIVERSAL::isa($val, 'HASH');

            return unless grep { $_ eq 'resolve' } keys %{ $val };
            return unless grep { $_ eq 'type' } keys %{ $val };

            return 1;
        },
    },
};

=head1 NAME

Zaaksysteem::ZAPI::CRUD::Interface::Column - Single column definition for this interface

=head1 SYNOPSIS

=head1 DESCRIPTION

The CRUD Interface Column description for Angular

=head1 ATTRIBUTES

=head2 id [required]

ISA: String

Identifier for this column, must be unique within a single CRUD::Interface

=cut

has 'id'    => (
    is          => 'ro',
    isa         => 'Str',
    required    => 1,
);

=head2 label [required]

ISA: String

Human readable label for table header.

=cut

has 'label'    => (
    is          => 'ro',
    isa         => 'Str',
    required    => 1,
);

=head2 resolve [optional]

ISA: String

Tells the CRUD::Interface how to get the data from the JSON data. This is
normally the name of the key in a json hash. E.g.: zaaktype_node_id in a
zaak result.

=cut

has 'resolve'    => (
    is          => 'ro',
    isa         => 'Str',
);

=head2 sort [optional]

ISA: HashRef

 {
    type    => 'alphanumeric',
    resolve => 'zaaktype_node_id'
 }

A HashRef describing how to sort this column. The resolve parameter defines how
to get the data from the JSON object, see resolve above.

The type parameter defines the way this column can be sorted:

TODO DARIO

=over 4

=item alphanumeric

=back

=cut

has 'sort'    => (
    is          => 'ro',
    isa         => 'HashRef',
);

=head2 filter [optional]

ISA: String

Some given data needs to be converted to a human readable format on the front
side of the CRUD controller. You can show just the time when given a timestamp
etc.

The following filters could be used:

=over 4

=item date:'short'

 $col->filter("date:'short'");

Shows the short date for this filter

=back

=cut

has 'filter'    => (
    is          => 'ro',
    isa         => 'Str',
);

=head2 template [optional]

ISA: Str

An optional Angular template html can be given, to format column data in a table
when a filter is just not enough. Think of a progress filter when given a percentage.

Examples:

    $col->template('<zs-progress-bar progress="<[item.days_perc]>"></zs-progress-bar>')

    $col->template('<zs-progress-bar progress="<[item.days_perc]>"></zs-progress-bar>')

=cut

has 'template'    => (
    is          => 'ro',
    isa         => 'Str',
);

=head2 dynamic [optional]

ISA: Bool

Indicates whether or not the column is dynamic. Dynamic
means the bindings are expected to change and need to be reinterpreted on
$scope.$apply(). When undefined, it defaults to false.

=cut

has 'dynamic'    => (
    is          => 'ro',
    isa         => 'Bool',
);

=head1 METHODS

=head2 TO_JSON

Send data in json format

=cut

sub TO_JSON {
    my $self        = shift;

    $self->_validate;

    return $self->_object_params;
}

=head1 INTERNAL METHODS

=head2 _object_params

Returns a HASHRef containing this object parameters according to this object
profile

=cut

sub _object_params {
    my $self        = shift;

    return { map {
        $_ => $self->$_
    } (
        @{ OBJECT_PROFILE->{required} },
        @{ OBJECT_PROFILE->{optional} }
    ) };
}

=head2 _validate

Validates this object, to prove it is complete and contains valid attributes

=cut

sub _validate {
    my $self            = shift;

    my $dv              = Data::FormValidator->check(
        $self->_object_params,
        OBJECT_PROFILE
    );

    die(
        'Cannot validate form field, invalid or missing: '
        . join(',', $dv->invalid, $dv->missing)
    ) unless $dv->success;

    return 1;

}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::ZAPI::CRUD::Interface> L<Zaaksysteem::Manual> L<Zaaksysteem::Manual::Template>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 OBJECT_PROFILE

TODO: Fix the POD

=cut

