package Zaaksysteem::SBUS::ResultSet::Bedrijf;

use Moose;

extends qw/DBIx::Class::ResultSet Zaaksysteem::SBUS::ResultSet::GenericImport/;

use Zaaksysteem::Constants;
use Zaaksysteem::SBUS::Constants;
use Zaaksysteem::SBUS::Logging::Object;
use Zaaksysteem::SBUS::Logging::Objecten;

use Data::Dumper;

use constant IMPORT_KERNGEGEVEN_LABEL   => 'dossiernummer';

with qw/Zaaksysteem::BR::Subject::ResultSet::Company/;


has '_import_kerngegeven_label'  => (
    'is'        => 'ro',
    'lazy'      => 1,
    default     => sub {
        return IMPORT_KERNGEGEVEN_LABEL;
    }
);

has '_import_objecttype'  => (
    'is'        => 'ro',
    'lazy'      => 1,
    default     => sub {
        return SBUS_LOGOBJECT_PRS;
    }
);

has '_import_entry_profile' => (
    'is'        => 'ro',
    'lazy'      => 1,
    default     => sub {
        return GEGEVENSMAGAZIJN_KVK_PROFILE;
    }
);

sub _get_kern_record {
    my ($self, $params, $options) = @_;

    ### Check against database
    my $records = $self->search(
        {
            dossiernummer               => $params->{dossiernummer},
            subdossiernummer            => $params->{subdossiernummer},
            vestiging_straatnaam        => $params->{vestiging_straatnaam},
            vestiging_huisnummer        => $params->{vestiging_huisnummer},
            authenticated               => 1,
        }
    );

    die('FOUND MORE THAN 1 ENTRY, CANNOT CONTINUE') if (
        $records->count && $records->count > 1
    );

    return $records->first;
}


sub _delete_real_entry {
    my ($self, $params, $options) = @_;

    my $record = $self->_get_kern_record($params, @_);

    $record->deleted_on(DateTime->now());
    $record->update;
}

sub _import_real_entry {
    my ($self, $params, $options) = @_;
    my ($record, $adres_record);

    if (uc($options->{mutatie_type}) =~ /W|V/) {
        $record = $self->_get_kern_record($params, @_);

        return unless $record;
    } else {
        $record = $self->create(
            {
                dossiernummer   => $params->{dossiernummer},
                handelsnaam     => $params->{handelsnaam},
            }
        );
    }

    ### Detect changes
    $self->_detect_changes($params, $options, $record);

    $params->{authenticated}    = 1;
    $params->{authenticatedby}  = 'kvk';

    $record         ->update($params);
}

=head2 find_by_kvk_and_vestiging

Find a company based on kvk and vestigingsnummer

=cut

sub find_by_kvk_and_vestiging {
    my $self    = shift;
    my $options = shift;

    my $kvk       = delete $options->{kvk};
    my $vestiging = delete $options->{vestigingsnummer};

    my $rs = $self->search_rs(
        {
            'dossiernummer::bigint'                  => int($kvk),
            'NULLIF(vestigingsnummer, null)::bigint' =>
                $vestiging ? int($vestiging) : undef,
            deleted_on => undef,
            authenticated => 1,
            %$options,
        }
    );

    my $bedrijf = $rs->next;
    if ($rs->next) {
        throw('bedrijf/multiple',
            'Found multiple bedrijf rows for given kvk/vestigingsnummer');
    }
    return $bedrijf;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 GEGEVENSMAGAZIJN_KVK_PROFILE

TODO: Fix the POD

=cut

=head2 IMPORT_KERNGEGEVEN_LABEL

TODO: Fix the POD

=cut

=head2 SBUS_LOGOBJECT_PRS

TODO: Fix the POD

=cut

