package Zaaksysteem::XML::Reader;
use Moose;
with 'MooseX::Log::Log4perl';
use namespace::autoclean;

=head1 NAME

Zaaksysteem::XML::Reader - An abstract reader class

=head1 DESCRIPTION

This module is able to read and parse a StUF-ZKN message

=head1 SYNOPSIS

    package Foo;
    use Zaaksysteem::StUF::ZKN::0102::Reader;

    my $xml = "<xml>here</xml>";
    my $reader = Zaaksysteem::StUF::ZKN::0102::Reader->new(
        xml => $xml;
    );

    $reader->methods;

=cut

use Zaaksysteem::Types qw(NonEmptyStr);
use BTTW::Tools;

=head1 ATTRIBUTES

=head2 xml

The XML string you want to parse

=cut

has xml => (
    is       => 'ro',
    isa      => NonEmptyStr,
    required => 1,
);

=head2 xpath

An L<XML::LibXML::XPathContext> object, build by C<_build_xpath>

=cut

has xpath => (
    is      => 'ro',
    isa     => 'XML::LibXML::XPathContext',
    lazy    => 1,
    builder => '_build_xpath',
);

=head1 METHODS

=head2 has_section

Check if the XML has a given section

    $self->has_section("Section name", "/xpath/query");

=cut

sig has_section => 'Str,Str';

sub has_section {
    my ($self, $section, $xpath) = @_;

    my $node_list = $self->xpath->findnodes($xpath);
    return $node_list if $node_list->size;
    return 0;
}

=head2 assert_section

Assert if the XML has a given section

    $self-assert_section("Section name", "/xpath/query");

=cut

sig assert_section => 'Str,Str';

sub assert_section {
    my ($self, $section, $xpath) = @_;

    if (my $list = $self->has_section($section, $xpath)) { 
        return $list;
    }
    else {
        throw(
            'xml/reader/section/missing',
            "Missing section $section / $xpath"
        );
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
