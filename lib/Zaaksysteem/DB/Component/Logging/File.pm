package Zaaksysteem::DB::Component::Logging::File;

use Moose;

has file => ( is => 'ro', lazy => 1, default => sub {
    my $self = shift;

    $self->rs('File')->find($self->data->{ file_id });
});

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

