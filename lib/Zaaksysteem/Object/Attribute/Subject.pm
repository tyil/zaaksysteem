package Zaaksysteem::Object::Attribute::Subject;

use Moose::Role;

=head2 _build_human_value

Builds a human readable representation of a subject value, if present.

=cut

sub _build_human_value {
    my $self = shift;

    my $value = $self->value;

    return undef unless $value;

    my %map = (
        bedrijf => 'handelsnaam',
        natuurlijk_persoon => 'decorated_name'
    );

    return join(', ', map {
        $_->{ $map{ $_->{ object_type } } }
    } @{ $value });
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
