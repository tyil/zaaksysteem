package Zaaksysteem::Object::Roles::Type;

use Moose::Role;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::Object::Roles::Type - Role applied to all user-defined type
classes

=head1 DESCRIPTION

This role currently exists only to have a property on the user-defined type
instances that distinguish them from internal objects.

    $is_user_defined = $object->does('Zaaksysteem::Object::Roles::Type');

=cut

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
