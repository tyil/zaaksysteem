package Zaaksysteem::Object::Query::Expression::Inversion;

use Moose;

use Moose::Util::TypeConstraints qw[role_type];
use BTTW::Tools;

with 'Zaaksysteem::Object::Query::Expression';

=head1 NAME

Zaaksysteem::Object::Query::Expression::Inversion - Abstracts conditional
inversions (logical NOT)

=head1 DESCRIPTION

=head1 ATTRIBUTES

=head2 expression

The expression to be inverted.

=cut

has expression => (
    is => 'rw',
    isa => role_type('Zaaksysteem::Object::Query::Expression'),
    required => 1
);

=head1 METHODS

=head2 stringify

Implements logic required by
L<Zaaksysteem::Object::Query::Expression/stringify>.

    # "not my_field equal to 'abc'"
    qb_not(qb_eq('my_field', 'abc'))->stringify

=cut

sub stringify {
    my $self = shift;

    return sprintf('not %s', $self->expression->stringify);
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
