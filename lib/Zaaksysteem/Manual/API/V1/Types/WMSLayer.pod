=head1 NAME

Zaaksysteem::Manual::API::V1::Types::WMSLayer - Type definition for WMS map layer objects

=head1 DESCRIPTION

This page documents the serialization of C<wms_layer> objects.

=head1 JSON

=begin javascript

{
    "type": "wms_layer",
    "reference": null,
    "instance": {
        "layer_name": "foo",
        "url": "https://wms.example.com/mappingservice",
        "label": "My First Map",
        "active": true
    }
}

=end javascript

=head1 INSTANCE ATTRIBUTES

=head2 layer_name E<raquo> L<C<string>|Zaaksysteem::Manual::API::V1::ValueTypes/string>

Layer name (as used in the C<< <Layer> >> tag in response to the
C<GetCapabilities> call of the WMS server.

=head2 url E<raquo> L<C<string>|Zaaksysteem::Manual::API::V1::ValueTypes/string>

Base-URL for the WMS mapping service this map layer is available on.

=head2 label E<raquo> L<C<string>|Zaaksysteem::Manual::API::V1::ValueTypes/string>

A human-readable name for this map layer.

=head2 active E<raquo> L<C<boolean>|Zaaksysteem::Manual::API::V1::ValueTypes/boolean>

Boolean value, indicating whether the layer is configured to be active.

=head2 feature_info_xpath E<raquo> L<C<string>|Zaaksysteem::Manual::API::V1::ValueTypes/string>

XPath expression that can be used to extract the C<feature name> from the XML document
returned by a WMS request for feature information.

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
