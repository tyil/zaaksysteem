#!/bin/sh

if [ -n "$ZS_PRE_ACTION" ] && [ -x "$ZS_PRE_ACTION" ]
then
    "$ZS_PRE_ACTION"
fi

exec "$@"
