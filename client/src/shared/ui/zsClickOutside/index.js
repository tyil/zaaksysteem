import angular from 'angular';

export default
	angular.module('zsClickOutside', [

	])
		.directive('zsClickOutside', [ '$parse', '$document', ( $parse, $document ) => {

			return {
				restrict: 'A',
				link: ( scope, element, attrs ) => {

					let onClickOutside = $parse(attrs.zsClickOutside),
						onClickOutsideEvent = attrs.onClickOutsideEvent || 'mousedown';

					$document.bind(onClickOutsideEvent, ( event ) => {

						if (!(element[0] === event.target || element[0].contains(event.target))) {

							let shouldApply = onClickOutside(scope, { $event: event });
							
							// let the click handler decide if $apply should be called
							if (shouldApply !== false) {
								scope.$apply();
							}
						}

					});

				}
			};

		}])
		.name;
