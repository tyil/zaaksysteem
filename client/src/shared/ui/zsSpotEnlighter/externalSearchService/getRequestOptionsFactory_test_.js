import { expect } from 'chai';
import getRequestOptionsFactory from './getRequestOptionsFactory';

const getRequestOptions = getRequestOptionsFactory(() => ({
	endpoint: 'foo',
	token: {
		access_token: 'quux'
	}
}));

describe('The getRequestOptionsFactory function', () => {

	it('works', () => {
		expect(getRequestOptions('/bar')).to.deep.equal({
			url: 'foo/bar',
			headers: {
				Authorization: 'Bearer quux',
				'X-Client-Type': undefined
			},
			timeout: 5000,
			withCredentials: false
		});
	});

});
