import angular from 'angular';
import resourceModule from './../../api/resource';
import first from 'lodash/head';

export default
	angular.module('sessionService', [
		resourceModule
	])
		.factory('sessionService', [ 'resource', ( resource ) => {

			let sessionService = {};

			sessionService.createResource = ( scope, options ) => {

				return resource(
					{
						url: '/api/v1/session/current',
						headers: {
							'Disable-Digest-Authentication': 1
						}
					},
					{
						scope,
						options
					}
				)
					.reduce( ( requestOptions, data ) => first(data));
			};

			return sessionService;

		}])
		.name;
