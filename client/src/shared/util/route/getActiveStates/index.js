export default ( current ) => {

	let parents = [ ],
		parent = current;

	while (parent) {
		parents.push(parent);
		parent = parent.parent;
	}

	return parents;

};
