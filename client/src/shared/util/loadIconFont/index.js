import FontFaceObserver from 'fontfaceobserver';

const FONT_NAME = 'Material Design Icons';
const iconFont = new FontFaceObserver(FONT_NAME);

const errorHandler = () => console.error(`${FONT_NAME} did not load.`);

const loadIconFont = () =>
	iconFont
		.load()
		.catch(errorHandler);

export default loadIconFont;
