export default ( phaseName ) => String(phaseName || '').replace(/[^a-z0-9]+/gi, '-').toLowerCase();

