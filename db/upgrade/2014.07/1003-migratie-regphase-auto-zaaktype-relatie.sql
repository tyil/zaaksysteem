-- This query fixes a legacy problem with casetype settings.
-- Any subcase that is configured for the registration phase needs
-- to be started automatically. Due to an HTML error this didn't
-- happen - A disabled checkbox indicated it would, but didn't
-- tell the backend.

BEGIN;

UPDATE zaaktype_relatie r SET automatisch_behandelen = true
FROM zaaktype_status s
WHERE s.id = r.zaaktype_status_id
AND s.status = 1;

COMMIT;

-- to check for success, these should be all true:
-- SELECT r.automatisch_behandelen FROM zaaktype_relatie r
-- JOIN zaaktype_status s ON s.id = r.zaaktype_status_id
-- WHERE s.status = 1;
