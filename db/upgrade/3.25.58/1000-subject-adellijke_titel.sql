BEGIN;

ALTER TABLE natuurlijk_persoon ADD COLUMN adellijke_titel TEXT;
ALTER TABLE gm_natuurlijk_persoon ADD COLUMN adellijke_titel TEXT;

COMMIT;
