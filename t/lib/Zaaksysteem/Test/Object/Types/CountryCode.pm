package Zaaksysteem::Test::Object::Types::CountryCode;

use Moose;
extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;

use Zaaksysteem::Object::Types::CountryCode;

sub test_new {
    my $country_code;
    lives_ok(
        sub {
            $country_code = Zaaksysteem::Object::Types::CountryCode
                ->new(
                    dutch_code  => 6030,
                    label       => 'Nederland',
                    alpha_two   => 'NL',
                    alpha_three => 'NLD',
                    code        => 528,
                )
        },
        'Creates a new object'
    );
    isa_ok($country_code, 'Zaaksysteem::Object::Types::CountryCode');
}

sub test_new_from_dutch_code {
    my $country_code;
    lives_ok(
        sub {
            $country_code = Zaaksysteem::Object::Types::CountryCode
                ->new_from_dutch_code( 9999 )
        },
        'Creates a new object from dutch_code'
    );
    is($country_code->label, 'Internationaal gebied', '... and is "Internationaal gebied"');

    undef $country_code;
    lives_ok(
        sub {
            $country_code = Zaaksysteem::Object::Types::CountryCode
                ->new_from_dutch_code( 0 )
        },
        'Creates a new object from zero'
    );
    is($country_code->label, 'Onbekend', '... and is "Onbekend"');

    undef $country_code;
    throws_ok(
        sub {
            $country_code = Zaaksysteem::Object::Types::CountryCode
                ->new_from_dutch_code( 999999999 )
        }, qr|object/types/countrycode/unknown_dutch_code|,
        'Can not create one with none existing number'
    );

}

sub test_new_from_name {
    my $country_code;
    lives_ok(
        sub {
            $country_code = Zaaksysteem::Object::Types::CountryCode
                ->new_from_name( 'VATICAANSTAD' )
        },
        'Creates a new object from label "VATICAANSTAD"'
    );
    is($country_code->dutch_code, '5045', '... and has code "5045"');

    undef $country_code;
    throws_ok(
        sub {
            $country_code = Zaaksysteem::Object::Types::CountryCode
                ->new_from_name( 'Reunion' ) # Réunion
        }, qr|object/types/countrycode/unknown_name|,
        'Does need to match exactly'
    );
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Test::Object::Types::CountryCode; - Test CountryCode

=head1 DESCRIPTION

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::Object::Types::CountryCode;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
