package Zaaksysteem::Test::ZTT::Element;
use Moose;
extends 'Zaaksysteem::Test::Moose';

use JSON::XS;
use Zaaksysteem::Test;
use Zaaksysteem::Geo::BAG::Connection::Spoof;
use Zaaksysteem::Geo::BAG::Model;
use Zaaksysteem::Geo::BAG::Object;
use Zaaksysteem::ZTT::Element;

sub test_sanitize {
    my $conn = Zaaksysteem::Geo::BAG::Connection::Spoof->new(
        home => "$FindBin::Bin/..",
        priority_gemeentes => [ 'Amsterdam' ],
    );
    my $bag_model = Zaaksysteem::Geo::BAG::Model->new(connection => $conn);

    my $mock_attribute = mock_one(
        result_source => sub {
            return mock_one(
                schema => sub {
                    return mock_one(
                        bag_model => sub { return $bag_model; }
                    );
                }
            )
        },
        library_attribute => sub {
            return mock_one( value_type => sub { return 'bag_adres' } );
        },
    );

    {
        my $ztt_elem = Zaaksysteem::ZTT::Element->new(
            attribute => $mock_attribute,
            value     => 'nummeraanduiding-109200000011544',
        );

        $ztt_elem->sanitize();

        is($ztt_elem->value, 'Paardelandsdrift 9, 7854PR Aalden', "Internal 'nummeraanduiding' value turned into human-readable address");
    }

    {
        my $ztt_elem = Zaaksysteem::ZTT::Element->new(
            attribute => $mock_attribute,
            value     => 'openbareruimte-109300000000499',
        );

        $ztt_elem->sanitize();

        is($ztt_elem->value, 'Aalden - Paardelandsdrift', "Internal 'openbareruimte' value turned into human-readable street name");
    }

    {
        my $ztt_elem = Zaaksysteem::ZTT::Element->new(
            attribute => $mock_attribute,
            value     => 'nummeraanduiding-109200000011544, nummeraanduiding-193200000033904',
        );

        $ztt_elem->sanitize();

        is(
            $ztt_elem->value,
            'Paardelandsdrift 9, 7854PR Aalden; Werkerlaan 103, 8043LV Zwolle',
            "Multi-value 'nummeraanduiding' value turned into human-readable addresses"
        );
    }

    {
        my $ztt_elem = Zaaksysteem::ZTT::Element->new(
            attribute => $mock_attribute,
            value     => 'openbareruimte-109300000000499, openbareruimte-193300000001426',
        );

        $ztt_elem->sanitize();

        is(
            $ztt_elem->value,
            'Aalden - Paardelandsdrift; Zwolle - Werkerlaan',
            "Multi-value 'openbareruimte' value turned into human-readable street names"
        );
    }
}

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
