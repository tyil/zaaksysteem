import {
    openPageAs
} from './../../../../functions/common/navigate';
import waitForElement from './../../../../functions/common/waitForElement';
import startForm from './../../../../functions/common/startForm';
import {
    goNext,
    addSubject
} from './../../../../functions/common/form';
import {
    openTab
} from './../../../../functions/intern/caseView/caseNav';
import {
    getContactRole,
    getContactAuthorisation
} from './../../../../functions/intern/caseView/caseRelations';

const relationsInfo = [
    {
        type: 'citizen normal',
        name: 'Z. Betrokkene toevoegen',
        subjectType: 'citizen',
        subjectToRelate: '948075272',
        role: 'Advocaat',
        authorisation: false
    },
    {
        type: 'organisation normal',
        name: 'Zaaktypeactie betrokkene toevoegen',
        subjectType: 'organisation',
        subjectToRelate: '02200002',
        role: 'Aannemer',
        authorisation: false
    },
    {
        type: 'citizen authorized',
        name: 'Z. Betrokkene toevoegen',
        subjectType: 'citizen',
        subjectToRelate: '948075272',
        role: 'Advocaat',
        authorisation: true
    },
    {
        type: 'citizen notified',
        name: 'Z. Betrokkene toevoegen',
        subjectType: 'citizen',
        subjectToRelate: '948075272',
        role: 'Advocaat',
        authorisation: true,
        notification: true
    },
    {
        type: 'citizen special role',
        name: 'Z. Betrokkene toevoegen',
        subjectType: 'citizen',
        subjectToRelate: '948075272',
        role: 'Slager',
        authorisation: false
    },
    {
        type: 'citizen manual role',
        name: 'Z. Betrokkene toevoegen',
        subjectType: 'citizen',
        subjectToRelate: '948075272',
        role: 'anders',
        roleOther: 'manual',
        authorisation: false
    }
];

relationsInfo.forEach(relationInfo => {
    const { type, name, role, roleOther, authorisation, notification } = relationInfo;

    describe(`when starting a registration form and adding a ${type} relation`, () => {
        beforeAll(() => {
            const data = {
                casetype: 'Zaaktypeactie betrokkene toevoegen',
                requestorType: 'citizen',
                requestorId: '1',
                channelOfContact: 'behandelaar'
            };
            
            openPageAs();
            startForm(data);
            goNext();
            addSubject(relationInfo);
            goNext();
            openTab('relations');
        });

        it('the case should have the subject as a relation', () => {
            expect($('.related_subjects').getText()).toContain(name); 
        });

        it('the subject should have the selected role', () => {
            const roleResult = roleOther || role;

            expect(getContactRole(name)).toEqual(roleResult);
        });

        it(`the subject authorisation should be set to ${authorisation}`, () => {
            expect(getContactAuthorisation(name)).toBe(authorisation);
        });

        if ( notification ) {
            it('the subject should have been notified', () => {
                browser.ignoreSynchronization = true;
                openTab('timeline');
                browser.sleep(5000);
                expect($('[data-event-type="email/send"]').isPresent()).toBe(notification);
                browser.get('/intern/');
                browser.ignoreSynchronization = false;
            });
        }
    });
});


/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
