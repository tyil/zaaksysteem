/*global angular,_*/
(function ( ) {

	angular.module('Zaaksysteem.form')
		.directive('zsMapFormField', [ function ( ) {

			return {
				require: [ 'ngModel', 'zsEzraMap' ],
				link: function ( scope, element, attrs, controllers ) {

					var ngModel = controllers[0],
						zsEzraMap = controllers[1];

					function setModelValue ( coordinates ) {
						var value = coordinates.lat + ',' + coordinates.lon;
						ngModel.$setViewValue(value);
					}

					function setViewValue ( ) {

						if(!zsEzraMap.isLoaded()) {
							return;
						}

						var modelValue = ngModel.$modelValue,
							markers;
						if(modelValue && !_.isArray(modelValue)) {
							modelValue = [ modelValue ];
						}

						if(!modelValue) {
							modelValue = [];
						}

						markers = modelValue
							.map(function ( str ) {
								var coordinates = str.split(','),
									lat = Number(coordinates[0]),
									lng = Number(coordinates[1]),
									epsg = zsEzraMap.fromLatLngToGps(lat, lng);

								return {
									longitude: epsg.x,
									latitude: epsg.y,
									popup_data: {
										coordinates: {
											lat: lat,
											lon: lng
										},
										address: str
									},
									identification: str,
									no_update: 1,
									center: true
								};
							});

						zsEzraMap.setMarkers(markers);
					}

					zsEzraMap.onSelect.push(setModelValue);

					zsEzraMap.onLoad.then(setViewValue);

					ngModel.$formatters.push(function ( value ) {
						setViewValue();
						return value;
					});
				}

			};

		}]);

})();
