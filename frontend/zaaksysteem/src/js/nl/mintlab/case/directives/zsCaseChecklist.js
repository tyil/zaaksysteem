/*global angular,_*/
(function ( ) {
	
	angular.module('Zaaksysteem.case')
		.directive('zsCaseChecklist', [ '$http', 'systemMessageService', function ( $http, systemMessageService ) {
			
			return {
				require: [ 'zsCaseChecklist', '^zsCaseSidebar', '^zsCaseView', '^zsCasePhaseView' ],
				controller: [ function ( ) {
					var ctrl = this,
						zsCaseSidebar,
						zsCaseView,
						zsCasePhaseView;
					
					ctrl.setControllers = function ( zsCaseSidebarCtrl, zsCaseViewCtrl, zsCasePhaseViewCtrl ) {
						zsCaseSidebar = zsCaseSidebarCtrl;
						zsCaseView = zsCaseViewCtrl;
						zsCasePhaseView = zsCasePhaseViewCtrl;
					};
					
					ctrl.addItem = function ( label ) {
						var item = {
								id: -1,
								label: label,
								user_defined: true,
								checked: false
							};
						
						ctrl.getChecklistItems().push(item);
						
						$http({
							method: 'POST',
							url: '/zaak/' + zsCaseView.getCaseId() + '/checklist/add_item',
							data: {
								label: label,
								milestone: zsCasePhaseView.getMilestone()
							}
						})
							.success(function ( data ) {
								var itemData = data.result[0];
								for(var key in itemData) {
									item[key] = itemData[key];
								}
							})
							.error(function ( ) {
								systemMessageService.emitError('Het checklist item kon niet worden aangemaakt. Probeer het later opnieuw.');
								_.pull(ctrl.getChecklistItems(), item);
							});
					};
					
					ctrl.removeItem = function ( item ) {
						
						_.pull(ctrl.getChecklistItems(), item);
						
						$http( {
							method: 'POST',
							url: '/zaak/' + zsCaseView.getCaseId() + '/checklist/remove_item',
							data: {
								item_id: item.id
							}
						})
							.error(function ( /*data*/ ) {
								systemMessageService.emitError('Het checklist item kon niet worden verwijderd. Probeer het later opnieuw.');
								ctrl.getChecklistItems().push(item);
							});
					};
					
					ctrl.updateItem = function ( item ) {
						
						var data = {
							
						};
						
						if(zsCaseSidebar.isClosed()) {
							item.checked = !item.checked;
							return;
						}
						
						data[item.id] = item.checked;
						
						$http( {
							method: 'POST',
							url: '/zaak/' + zsCaseView.getCaseId() + '/checklist/update',
							data: {
								data: data
							}
						})	
							.error(function ( ) {
								systemMessageService.emitSaveError('checklist item');
							});
					};
					
					ctrl.getChecklistItems = function ( ) {
						return zsCaseSidebar.getChecklistItems();	
					};
					
					ctrl.isClosed = function ( ) {
						return zsCaseSidebar.isClosed();	
					};
					
					return ctrl;
				}],
				controllerAs: 'caseChecklist',
				link: function ( scope, element, attrs, controllers ) {
					
					controllers[0].setControllers.apply(controllers[0], controllers.slice(1));
					
				}
			};
			
		}]);
	
})();
